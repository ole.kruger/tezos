(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This module provides different handlers related to DAL slots. *)

(* We cannot include a raw mli file. But this will be removed once full
   migration is done. *)
include module type of Slot_manager_legacy

type error += Invalid_slot_size of {provided : int; expected : int}

(** [add_slots slot node_Store cryptobox] computes the given [slot]'s commitment
    and adds the association "commitment -> slot" in the DAL's [node_store] if
    the commitment is not already bound to some data.

    The function returns an error {!ref:Invalid_slot_size} if the [slot]'s size
    doesn't match the expected slots' size given in [cryptobox], or the [slot]'s
    commitment otherwise.
*)
val add_slots :
  Cryptobox.slot ->
  Store.node_store ->
  Cryptobox.t ->
  Cryptobox.commitment tzresult Lwt.t

(** [add_slot_id commitment slot_id node_Store cryptobox] associates a [slot_id]
    to a [commitment] in [node_store]. The function returns [Error `Not_found]
    if there is no entry for [commitment] in [node_store]. Otherwise, [Ok ()]
    is returned.
*)
val add_slot_id :
  Cryptobox.commitment ->
  Services.Types.slot_id ->
  Store.node_store ->
  Cryptobox.t ->
  (unit, [> `Not_found]) result Lwt.t
